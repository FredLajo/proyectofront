import { LitElement, html } from 'lit-element';
import Navigo from 'https://unpkg.com/navigo@7.1.2/lib/navigo.es.js';



class CuentaData extends LitElement {
  static get properties() {
    return {
      accountID: {type: String},
      accountNumber:{type:String},
      amountC:{type:String},
      amountD:{type:String},
      description:{type:String},
      cci:{type:String},
      divisa:{type:String},
      data: {type: Object}
    }
  }
  constructor(){
    super()
    this.data = [];
  }
  render() {
    return html`
    <style>
    .caja{
      box-sizing: border-box;
      height: 80px;
      width: 100%;
      border:1px solid var(--lumo-contrast-20pct);
      display:flex;
      flex-flow: row wrap;
      font-size: 14px;
    }
    .caja:hover{
      background-color: rgba(114,199,210,0.8);
    }
    #cuenta{
      width: 60%;
      align-items: flex-start;
      padding-left: 20px;
    }
    #saldo{
      width: 40%;
      padding-right: 30px;
      align-items: flex-end;
    }
    .flex{
      height: 100%;
      display:flex;
      flex-flow: column nowrap;
      justify-content: center;
      box-sizing: border-box;
    }
    .blue{
      color:blue;
    }
    .hidden {
        opacity: 0;
        transition: opacity 1s, height 0 1s;
        height: 0;
        display:none;
    }
    .fondo{
      background-color: rgba(114,199,210,0.8);
    }
    </style>
      <div id="cuentapanel" class="caja" @click="${this.transacciones}" >
          <div id="cuenta" class="flex">
            <div class="blue" style="font-size:15px;">${this.description}</div>
            <div>${this.accountNumber}</div>
          </div>
          <div id="saldo" class="flex">
            <div class="blue">Contable</div>
            <div>${this.divisa=='PEN'?'S/.':'$/.'} ${this.amountC}</div>
            <div class="blue">Disponible</div>
            <div>${this.divisa=='PEN'?'S/.':'$/.'} ${this.amountD}</div>
          </div>
      </div>
      <div id="transactionpanel" class="hidden" >
      <div className="sweet-loading">
        <ClipLoader
          css={override}
          size={150}
          color={"#123abc"}
          loading={this.state.loading}
        />
      </div>
        ${this.data.map(datos => html`
            <transaction-data
              transacID="${datos.transacID}"
              description="${datos.description}"
              amount="${datos.amount}"
              date="${datos.date}"
              tipo="${datos.tipo}"
            > </transaction-data>
        `)}
      </div>
    `
  }
  transacciones(){
    let trans = '[{"transacID":1,"description":"abono en cuenta 1966-7711-71-6020072316","amount":326.1,"date":"3/9/2020","tipo":"cargo"},'+
                '{"transacID":2,"description":"abono en cuenta 1966-7711-71-6020072316","amount":326.1,"date":"3/9/2020","tipo":"cargo"},'+
                '{"transacID":3,"description":"abono en cuenta 2415-3279-87-7660582932","amount":355.84,"date":"1/19/2020","tipo":"abono"}]';
    this.data=JSON.parse(trans);

    this.shadowRoot.getElementById("cuentapanel").classList.toggle('fondo');
    this.shadowRoot.getElementById("transactionpanel").classList.toggle('hidden');

  }
}

customElements.define('cuenta-data', CuentaData)
