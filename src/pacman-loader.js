import { LitElement, html } from 'lit-element';


class PacmanLoader extends LitElement {
  constructor(){
    super()
  }
  render() {
    return html`
    <style>
        body{margin:0;background:#1C163A}
        .loader{position:absolute;top:50%;left:50%;height:60px;width:160px;margin:0;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}
        .circles{position:absolute;left:-5px;top:0;height:60px;width:220px}
        .circles span{position:absolute;top:25px;height:15px;width:15px;border-radius:12px;background-color:#EFEFEF}
        .circles span.one{right:80px}
        .circles span.two{right:40px}
        .circles span.three{right:0px}
        .circles span.four{right:120px}
        .circles{-webkit-animation:animcircles 0.5s infinite linear;animation:animcircles 0.5s infinite linear}
        @-webkit-keyframes animcircles{0%{-webkit-transform:translate(0px,0px);transform:translate(0px,0px)}100%{-webkit-transform:translate(-40px,0px);transform:translate(-40px,0px)}}
        @keyframes animcircles{0%{-webkit-transform:translate(0px,0px);transform:translate(0px,0px)}100%{-webkit-transform:translate(-40px,0px);transform:translate(-40px,0px)}}
        .pacman{position:absolute;left:0;top:0;height:60px;width:60px}
        .pacman .eye{position:absolute;top:10px;left:30px;height:7px;width:7px;border-radius:7px;background-color:#1C163A}
        .pacman span{position:absolute;top:0;left:0;height:60px;width:60px}
        .pacman span::before{content:"";position:absolute;left:0;height:30px;width:60px;background-color:#FFFB16}
        .pacman .top::before{top:0;border-radius:60px 60px 0px 0px}
        .pacman .bottom::before{bottom:0;border-radius:0px 0px 60px 60px}
        .pacman .left::before{bottom:0;height:60px;width:30px;border-radius:60px 0px 0px 60px}
        .pacman .top{-webkit-animation:animtop 0.5s infinite;animation:animtop 0.5s infinite}
        @-webkit-keyframes animtop{0%,100%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}50%{-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}}
        @keyframes animtop{0%,100%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}50%{-webkit-transform:rotate(-45deg);transform:rotate(-45deg)}}
        .pacman .bottom{-webkit-animation:animbottom 0.5s infinite;animation:animbottom 0.5s infinite}
        @-webkit-keyframes animbottom{0%,100%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}50%{-webkit-transform:rotate(45deg);transform:rotate(45deg)}}
        @keyframes animbottom{0%,100%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}50%{-webkit-transform:rotate(45deg);transform:rotate(45deg)}}
    </style>
    <div class="loaderr">
      <div class="circles">
        <span class="one"></span>
        <span class="two"></span>
        <span class="three"></span>
        <span class="four"></span>
      </div>
      <div class="pacman">
        <span class="top"></span>
        <span class="bottom"></span>
        <span class="left"></span>
      </div>
      </div>
    `
  }
}

customElements.define('pacman-loader', PacmanLoader)

class PacmanLoader2 extends LitElement {
  render() {
    return html`
    <style>
    @import url(https://fonts.googleapis.com/css?family=Open+Sans:300);
@keyframes leg {
0% {
  transform: translateX(0);
}

25% {
  transform: translateX(10%);
}

50% {
  transform: translateX(0%);
}

75% {
  transform: translateX(-10%);
}

100% {
  transform: translate(0);
}

}
@keyframes move{
0%{
  transform: translateX(0);
}

50%{
  transform: translateX(20px);
}

100% {
  transform: translateX(0);
}
}
@keyframes colorChange{
50%{
  background-color: #1F3A93;
}
}
@keyframes pupil{
0%{transform: translateX(0);}
25% {transform: translateX(10%);}
49% {transform: translateX(10%); opacity: 1;}
50% {transform: translateX(10%); opacity: 0;}
100% {opacity: 0; }
}
@keyframes bottomChomp{
0% {
  transform: rotate(30deg);
}

50%{
  transform: rotate(-45deg);
}

100% {
  transform: rotate(30deg);
}
}
@keyframes topChomp{
0% {
  transform: rotate(-30deg);
}

50%{
  transform: rotate(30deg);
}

100% {
  transform: rotate(-30deg);
}
}
@keyframes spin {
0% {
  transform: rotate(0deg);
}

50% {
  transform: rotate(-180deg);
}

100% {
  transform: rotate(0deg);
}
}
#pantallaCarga{
    z-index: 800;
    content: "";
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    pointer-events: auto;
    display: flex;
    justify-content:center;
    align-items: center;

  }
.container{
  z-index: 900;
  width: 275px;
  height: 95px;
  display:flex;
  align-items: center;
  justify-content: center;

  position: absolute;
  border-radius:15px;
  border: 3px solid red;
  background-color: black;
  /*box-shadow: 0px 0px 13px 2px blue;*/
}
.loader-container {
/*box-shadow: 0px 0px 13px 2px blue;*/
text-align: center;
color: #fff;
font-family: 'Open Sans';
width: 250px;
height: 70px;
background-color: black;
display:flex;
align-items: center;
justify-content: center;
border-radius:10px;
border: 3px solid red;
flex-flow:column nowrap;
}
.animation-container {
/*margin-left: -160px;
background-color: green;*/
}
.ghost-container {
height: 29px;
width:29px;
display: inline-block;
animation: move 3s linear 0s infinite;
}
.pac-man-move-container {
margin-left: 30px;
display: inline-block;
animation: move 3s linear 0s infinite;
}

.pac-man-container {
height 30px;
width 30px;
display: inline-block;
animation: spin 3s cubic-bezier(1, 0.05, 1,-0.32) 0s infinite;
}

.pac-man-top, .pac-man-bottom {
border: 14px solid yellow;
border-radius: 50%;
border-right-color: transparent;
}


.pac-man-top {
border-bottom-color: transparent;
animation: topChomp 0.3s linear 0s infinite;
}

.pac-man-bottom {
margin-top: -100%;
border-top-color: transparent;
animation: bottomChomp 0.3s linear 0s infinite;
}

.red .ghost-body, .red .ghost-leg {
background-color: red;
}


.blue .ghost-body, .blue .ghost-leg {
background-color: cyan;
}

.pink .ghost-body, .pink .ghost-leg {
background-color: pink;
}

.orange .ghost-body, .orange .ghost-leg{
background-color: orange;
}

.ghost-body{
height: 70%;
border-radius: 100% 100% 0 0;
animation: colorChange 3s cubic-bezier(1, 0.05, 1,-0.32) 0s infinite;
}

.ghost-legs-container{
position: relative;
height: 30%;
overflow: hidden;
}

.ghost-leg {
width: 33.333%;
height: 100%;
float: left;
display: inline-block;
border-radius: 0 0 50% 50%;
animation: leg 0.8s linear 2s infinite, colorChange 3s cubic-bezier(1, 0.05, 1,-0.32) 0s infinite;
}



.eyes-container {
position: relative;
display: flex;
justify-content: space-between;
height: 40%;
top: 30%;
margin: 0 10%;
}

.eye {
position: relative;
display: flex;
align-items: center;
justify-content: center;
float: left;
background-color: #fff;
height: 100%;
width: 40%;
border-radius: 50%;
overflow: hidden;
}

.eye > .pupil {
position: relative;
background-color: black;
height: 70%;
width: 70%;
border-radius: 50%;
animation: pupil 3s linear 0s infinite;
}
    </style>
    <div id="pantallaCarga">
    <div class="container">
    <section class="loader-container">
<div class="animation-container">
<div class="ghost-container red">
  <div class="ghost-body">
    <div class="eyes-container">
      <div class="eye">
        <div class="pupil">
        </div>
      </div>
      <div class="eye">
        <div class="pupil">
        </div>
      </div>
    </div>
  </div>
  <div class="ghost-legs-container">
  <div class="ghost-leg leg-1">
  </div>
  <div class="ghost-leg leg-2">
  </div>
  <div class="ghost-leg leg-3">
  </div>
  </div>
</div>
<div class="ghost-container pink">
  <div class="ghost-body">
    <div class="eyes-container">
      <div class="eye">
        <div class="pupil">
        </div>
      </div>
      <div class="eye">
        <div class="pupil">
        </div>
      </div>
    </div>
  </div>
  <div class="ghost-legs-container">
  <div class="ghost-leg leg-1">
  </div>
  <div class="ghost-leg leg-2">
  </div>
  <div class="ghost-leg leg-3">
  </div>
  </div>
</div>
<div class="ghost-container blue">
  <div class="ghost-body">
    <div class="eyes-container">
      <div class="eye">
        <div class="pupil">
        </div>
      </div>
      <div class="eye">
        <div class="pupil">
        </div>
      </div>
    </div>
  </div>
  <div class="ghost-legs-container">
  <div class="ghost-leg leg-1">
  </div>
  <div class="ghost-leg leg-2">
  </div>
  <div class="ghost-leg leg-3">
  </div>
  </div>
</div>
<div class="ghost-container orange">
  <div class="ghost-body">
    <div class="eyes-container">
      <div class="eye">
        <div class="pupil">
        </div>
      </div>
      <div class="eye">
        <div class="pupil">
        </div>
      </div>
    </div>
  </div>
  <div class="ghost-legs-container">
  <div class="ghost-leg leg-1">
  </div>
  <div class="ghost-leg leg-2">
  </div>
  <div class="ghost-leg leg-3">
  </div>
  </div>
</div>
<div class="pac-man-move-container">
<div class="pac-man-container">
  <div class="pac-man-top">
  </div>
  <div class="pac-man-bottom">
  </div>
  </div>
</div>
</div>
<div>Loading ...<div>
</section>
</div>
</div>
    `
  }
}

customElements.define('pacman-loader2', PacmanLoader2)
