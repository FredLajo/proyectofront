import { LitElement, html } from 'lit-element';

class TransactionData extends LitElement {
  static get properties() {
    return {
      transacID: {type: String},
      description:{type:String},
      amount:{type:Number},
      date:{type:String},
      tipo:{type:String}
    }
  }
  constructor(){
    super();
  }
  render() {
    return html`
    <style>
    .caja{
      box-sizing: border-box;
      height: 50px;
      width: 100%;
      border:1px solid var(--lumo-contrast-20pct);
      border-top: 0px;
      border-bottom: 0px;
      display:flex;
      flex-flow: row wrap;
      font-size: 14px;
      background-color: rgba(114,199,210,0.5);
    }
    #descripcion{
      width: 70%;
      align-items: flex-start;
      padding-left: 20px;
      overflow:hidden;
    }
    #monto{
      width: 30%;
      padding-right: 30px;
      align-items: flex-end;
      font-size: 17px;
    }
    .flex{
      height: 100%;
      display:flex;
      flex-flow: column nowrap;
      justify-content: center;
      box-sizing: border-box;
    }
    .rojo{
      color:red;
    }
    .blue{
      color:blue;
    }
    </style>
      <div class="caja">
        <pacman-loader2></pacman-loader2>
        <div id="descripcion" class="flex">
          <div style="text-overflow: ellipsis;">${this.description}</div>
          <div style="font-size:12px;">${this.date}</div>
        </div>
        <div id="monto" class="flex">
          <div class="${this.tipo=='cargo'?'rojo':'blue'}">${this.amount}</div>
        </div>
      </div>
    `
  }
}

customElements.define('transaction-data', TransactionData)
