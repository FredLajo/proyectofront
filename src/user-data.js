import { LitElement, html } from 'lit-element';


class UserData extends LitElement {
  static get properties() {
    return {
      userID:{type:String},
      name: { type: String },
      apepat: { type: String },
      apemat: { type: String },
      datas: {type: Object},
      fecha:{type:String}
    }
  }
  constructor(){
    super();
    /*var txt = '[{"id":1,"account":"8927-1055-51-9492763198", "saldoc":5993.25, "saldod":7838.73},'+
              '{"id":2,"account":"1111-1055-51-9492763198", "saldoc":5993.25, "saldod":7838.73},'+
              '{"id":3,"account":"1111-1055-51-9492763198", "saldoc":5993.25, "saldod":7838.73},'+
              '{"id":4,"account":"1111-1055-51-9492763198", "saldoc":5993.25, "saldod":7838.73},'+
              '{"id":5,"account":"2222-1055-51-9492763198", "saldoc":5993.25, "saldod":7838.73}]';
    var obj = JSON.parse(txt);*/

    var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    var f=new Date();
    this.fecha=diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear();
    this.datas = [];


  }
  render() {

    return html`
    <style>
    span{
      color:blue;

    }

    #data1{
      margin-top: 10px;
      margin-bottom: 10px;
    }
    #encabezadotabla{
      height:20px;
      border:1px solid var(--lumo-contrast-20pct);
      color:white;
      padding:15px;
      border-radius:5px 5px 0px 0px;
      border-bottom: 0px;
      background-color: #18749F;
      font-size: 16px;
    }
    </style>
    <custom-style>
      <style include="lumo-typography lumo-color">
    </style>
    </custom-style>
          <div style="text-align: right">${this.fecha}</div>
          <div style="margin-bottom:20px">
            <span><b>BIENVENIDO</b></span> ${this.name} ${this.apepat} ${this.apemat}
          </div>
          
          <div id="data1" >
            <div id="encabezadotabla">
              <b>CUENTAS</b>
            </div>
            ${this.datas.map(datos => html`
                <cuenta-data
                  accountID="${datos.accountID}"
                  description="${datos.description}"
                  accountNumber="${datos.cuenta}"
                  cci="${datos.cci}"
                  amountC="${datos.balance}"
                  amountD="${datos.balance}"
                  divisa="${datos.divisa}"
                > </cuenta-data>
            `)}
          </div>
          <iron-ajax @response="${this.bringdata}"
             auto url = "http://localhost:3000/api.peru/v1/users/${this.userID}/accounts"
             handle-as = "json">
          </iron-ajax>


    `

  }
  bringdata(data){
    console.log("aa");
    console.log(data.detail.response);
    this.datas=data.detail.response;
  }

}

customElements.define('user-data', UserData)
